#include "Pago.h"
#include <fstream>
#include <iostream>
#include <ctime>
#include <vector>
#include <dirent.h>
using namespace std;

bool is_file(string file){
	FILE *archivo;
	if(archivo = fopen(file.c_str(),"r")){
		fclose(archivo);
		return true;
	}else{
		return false;
	}
}

Pago::Pago(string m_nombre_archivo) : ListaSocios(m_nombre_archivo) {
	ActualizarVector();
}

void Pago::Prueba(){
	ifstream LogIngreso("Registro_Ingreso.dat",ios::binary|ios::ate);	
	int tam_bytes = LogIngreso.tellg();
	int cant_soc = tam_bytes/sizeof(reg_ingreso);
	cout << "-> " << cant_soc << endl;
	LogIngreso.seekg(0);
	for(int i=0;i<cant_soc;i++){ 
	LogIngreso.read(reinterpret_cast<char*>(&z),sizeof(z));
	cout << z.s_doc << "   " << z.dia_ing <<"/"<<z.mes_ing<<"/"<<z.anio_ing<<"  " <<z.hora_ing<<":"<<z.min_ing<<":"<<z.seg_ing<<endl;
	}
}
void Pago::Pagar(int socio, int plan){
	
	int dia,mes,anio;
	tie(dia,mes,anio) = Horario();
	
	x.s_socio=socio;
	x.s_plan=plan;
	x.dia_pago=dia; x.mes_pago=mes+1; x.anio_pago=anio+1900;
	ofstream ArchivoPagar2("Historial_Pagos2.dat",ios::binary|ios::app);
	ArchivoPagar2.write(reinterpret_cast<char*>(&x),sizeof(x));
	ArchivoPagar2.close();
	Pago("ListaSocios.dat");
}

void Pago::BorrarPagoSocio(int SocioBorrar){
	
	for(unsigned int i=0;i<v.size();i++) { 			/// Carga un nuevo vector sin los registro de pago del socio a borrar
		if(v[i].s_socio!=SocioBorrar){
			temp.push_back(v[i]);
		}
	}
	
	for(unsigned int i=0;i<temp.size();i++) { 						/// Actualiza la posicion de los socios mayores al eliminado
		if(temp[i].s_socio>SocioBorrar){
			temp[i].s_socio=temp[i].s_socio-1;}
	}
	
	ofstream ArchivoPagar2("Historial_Pagos2.dat",ios::binary|ios::trunc);			///Sobreescribe el archivo con el nuevo historial de pago
	for(unsigned int i=0;i<temp.size();i++){ 
		x.s_socio=temp[i].s_socio;
		x.s_plan=temp[i].s_plan;
		x.dia_pago=temp[i].dia_pago; x.mes_pago=temp[i].mes_pago; x.anio_pago=temp[i].anio_pago; 
		ArchivoPagar2.write(reinterpret_cast<char*>(&x),sizeof(x));
	}
	ArchivoPagar2.close(); 
}

void Pago::ConsultarUltimoPago(int socio){
	ActualizarVector();
	int cont = 0; int pos;
	for(unsigned int i=0;i<v.size();i++) { 
		if(v[i].s_socio == socio){
			cont++;
			pos = i;
		}
	}
	int dias = CalcularDiferencia(pos);
	if(cont!=0){cout << "El ultimo pago del socio " << v[pos].s_socio+1 << " fue el plan " << v[pos].s_plan << " hace " << dias <<" dias." 
		<< " --> " <<v[pos].dia_pago<<"/"<<v[pos].mes_pago<<"/"<<v[pos].anio_pago << endl;} 
	else { cout << "No hay registros de pago del socio indicado" << endl;} 
	
}

void Pago::ConsultarHistorialPago(int socio){
	ActualizarVector();
	int cont=0;
	for(unsigned int i=0;i<v.size();i++) {  
		if(v[i].s_socio == socio){
			cout << "Abono el plan " << v[i].s_plan << " el dia " << v[i].dia_pago<<"/"<<v[i].mes_pago<<"/"<<v[i].anio_pago << endl;
			cont++;
		}
	}
	if(cont == 0){ cout << "No hay registros de pago del socio indicado" << endl;
	}
}

int Pago::ConsultarPlan(int socio){
	ActualizarVector();
	int cont = 0; int pos;
	for(unsigned int i=0;i<v.size();i++) { 
		if(v[i].s_socio == socio){
			cont++;
			pos = i;
		}
	}
	return v[pos].s_plan;
}

tuple<int,int,int> Pago::Horario(){
	
	struct tm horaActual;
	time_t ahora;
	
	horaActual=*localtime(&ahora);
	
	return make_tuple(horaActual.tm_mday, horaActual.tm_mon, horaActual.tm_year);
}


int Pago::CalcularDiferencia(int socio){
	/// Hace cuantos dias pago por ultima vez
	struct tm horaActual;
	time_t ahora;
	ahora=time(NULL);
	horaActual=*localtime(&ahora);
	
	int cont = 0; int pos;
	for(unsigned int i=0;i<v.size();i++) { 			///Localiza el ultimo pago del socio
		if(v[i].s_socio == socio){
			cont++;
			pos = i;
		}
	}
	
	struct std::tm actual = {0,0,0,horaActual.tm_mday,horaActual.tm_mon+1,horaActual.tm_year}; /*Fecha actual*/
	struct std::tm ult_pago = {0,0,0,v[pos].dia_pago,v[pos].mes_pago,v[pos].anio_pago-1900}; /* Fecha de pago */
	std::time_t y = std::mktime(&actual);	
	std::time_t x = std::mktime(&ult_pago); double diferencia; 
	diferencia = std::difftime(y, x) / (60 * 60 * 24);
	
	return diferencia;
}


void Pago::ActualizarVector(){
	ifstream ArchivoPagar("Historial_Pagos2.dat",ios::binary|ios::ate);	
	v.erase(v.begin(),v.end());
	int tam_bytes = ArchivoPagar.tellg();
	int cant_soc = tam_bytes/sizeof(s_pago);
	ArchivoPagar.seekg(0);
	if(is_file("Historial_Pagos2.dat")==true){
		for(int i=0;i<cant_soc;i++) { 
			ArchivoPagar.read(reinterpret_cast<char*>(&x),sizeof(x));
			v.push_back(x);
		}
	}else{
		ofstream ArchivoPago("Historial_Pagos2.dat",ios::binary);
	}
	
}
tuple<int,int,int,int> Pago::Ingresar(string doc){
	struct tm horaActual;
	time_t ahora;
	ahora=time(NULL);
	horaActual=*localtime(&ahora);
	
	int diferencia,plan,dias_rest,socio;
	
	
	int y = DevolverNumeroSocio(doc);	
	socio = y;
	diferencia = CalcularDiferencia(y);		
	plan = ConsultarPlan(y);				
	
	string NombreIngreso = DevolverNombreSocio(y);
	/// Diferencia = hace cuanto pago
	if(plan == 1){ if(diferencia >= 30){ dias_rest = -1;} else{ dias_rest = 30-diferencia;}}
	if(plan == 2){ if(diferencia >= 90){ dias_rest = -1;} else{ dias_rest = 90-diferencia;}}
	if(plan == 3){ if(diferencia >= 365){ dias_rest = -1;} else{ dias_rest = 365-diferencia;}}
	
	
	ofstream LogIngreso("Registro_Ingreso.dat",ios::binary|ios::app);
	z.s_doc = doc;
	z.dia_ing=horaActual.tm_mday;  z.mes_ing=horaActual.tm_mon+1; z.anio_ing=horaActual.tm_year+1900;
	z.hora_ing=horaActual.tm_hour; z.min_ing=horaActual.tm_min; z.seg_ing=horaActual.tm_sec;
	
	LogIngreso.write(reinterpret_cast<char*>(&z),sizeof(z));
	LogIngreso.close();
	
	return make_tuple(socio,plan,diferencia,dias_rest);
}

void Pago::LeerRegistroIngreso(){
	ifstream LogIngreso("Registro_Ingreso.dat",ios::binary|ios::ate);
	int tam_bytes = LogIngreso.tellg();
	int cant_soc = tam_bytes/sizeof(reg_ingreso);
	cout << "-> " << cant_soc << endl;
	LogIngreso.seekg(0);
	if(is_file("Registro_Ingreso.dat") == true){
		for(int i=0;i<cant_soc;i++){ 
			LogIngreso.read(reinterpret_cast<char*>(&z),sizeof(z));
			cout << z.s_doc << "   " << z.dia_ing <<"/"<<z.mes_ing<<"/"<<z.anio_ing<<"  " <<z.hora_ing<<":"<<z.min_ing<<":"<<z.seg_ing<<endl;}
		}else{
			ofstream LogIngreso("Regsitro_Ingreso.dat",ios::binary);
		}
	}
bool Pago::EncontrarDni(string doc){
	int x = DevolverNumeroSocio(doc);
	if(x==-1){
		return false;
	}else{
		return true;
	}
}

void Pago::ConsultarPagosPorFecha(int c_diai,int c_mesi,int c_anioi,int c_diaf,int c_mesf,int c_aniof){
	ifstream ArchivoConsultarPagos("Historial_Pagos2.dat",ios::binary|ios::ate);	
	int tam_bytes = ArchivoConsultarPagos.tellg();
	int cant_soc = tam_bytes/sizeof(s_pago);
	ArchivoConsultarPagos.seekg(0);
	for(int i=0;i<cant_soc;i++) { 
		ArchivoConsultarPagos.read(reinterpret_cast<char*>(&x),sizeof(x));
		v.push_back(x);
	}
	int cantidad_pagos;
	
	for(int i=0;i<cant_soc;i++) { 
		if(v[i].mes_pago >= c_mesi and v[i].mes_pago <= c_mesf){
			if(v[i].dia_pago >= c_diai and v[i].dia_pago<=c_diaf){
				cantidad_pagos++;
			}
		}
	}
	cout<<cantidad_pagos<<endl;
	cout<<"DIA"<<"         PLAN"<<"         SOCIO"<<endl;
	for(int i=0;i<cantidad_pagos;i++) { 
		if(v[i].mes_pago >= c_mesi and v[i].mes_pago <= c_mesf){
			if(v[i].dia_pago >= c_diai and v[i].dia_pago<=c_diaf){
				if(v[i].anio_pago >= c_anioi and v[i].anio_pago<=c_aniof){
				cout<<v[i].dia_pago<<"           "<<v[i].s_plan<<"           "<<v[i].s_socio+1<<"      "<<v[i].dia_pago<<"/"<<v[i].mes_pago<<"/"<<v[i].anio_pago<<endl;
				}
			}
		}	
	}
}	

void Pago::ConsultarPagosPorPlan(int plan){
	ActualizarVector();
	cout << "Socios que abonaron el plan " << plan << ":" << endl;
	for(int i=0;i<v.size();i++) { 
		if(v[i].s_plan==plan){
			cout << v[i].s_socio+1 << " " << v[i].dia_pago <<"/"<<v[i].mes_pago<<"/"<<v[i].anio_pago<<endl;
		}
	}
}
