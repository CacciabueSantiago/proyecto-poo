#ifndef LISTASOCIOS_H
#define LISTASOCIOS_H
#include <vector>
#include "Persona.h"
using namespace std;

class ListaSocios {
private:
	string nombre_archivo;
	vector <Persona> ArregloPersonas;
public:
	ListaSocios(string archivo);
	bool Guardar();
	int CantidadDatos();
	void AgregarSocio(const Persona &p);
	void EliminarSocio(int i);
	
	int DevolverNumeroSocio(string dni);
	string DevolverNombreSocio(int soc);
	Persona &operator[](int i);
	
	Persona &VerPersona(int i);
	
	
	int BuscarNombre(std::string parte_nombre, int pos_desde);
	
	int BuscarApellido(std::string parte_apellido, int pos_desde);
	
	int BuscarApellidoYNombre(std::string parte_apellido, int pos_desde);
	
	int BuscarDireccion(std::string parte_direccion, int pos_desde);
	
	int BuscarTelefono(std::string parte_telefono, int pos_desde);
	
	int BuscarEmail(std::string parte_correo, int pos_desde);
	
	int BuscarCiudad(std::string parte_ciudad, int pos_desde);
	
	int BuscarNacimiento(int dia, int mes, int anio, int pos_desde);
	
};

#endif

