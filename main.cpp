#include "Persona.h"
#include <iostream>
#include <fstream>
#include "ListaSocios.h"
#include <vector>
#include <iomanip>
#include "Pago.h"
using namespace std;




int main(){
	
	ListaSocios Socios("ListaSocios.dat");  Pago y("ListaSocios.dat");
	for(int i=0;i<Socios.CantidadDatos();i++){
		Persona &p=Socios[i];
		cout<<"Socio " << i+1 << ": " << p.VerNombre()<< " " << p.VerApellido() << endl;}
	cout << endl;
	Persona new_socio;
	
	int menu; cout << "En que modo desea iniciar el programa?." ;
	cout << " " << " Presione 1 para modo usuario y 2 para modo administrador. " << endl;
	cin >> menu; string doc;
	if(menu == 1){
		do{
			int socio,plan,diferencia,dias_rest;
			cout << "Ingrese su numero de documento: "; cin >> doc;
			
			if(y.EncontrarDni(doc) == false){
				cout<<"No se encontro el registro ingresado. Intente nuevamente."<<endl;
			}else{
				tie(socio,plan,diferencia,dias_rest)=y.Ingresar(doc);	
				Persona &p=Socios[socio];
				cout << "Bienvenido " << p.VerNombre() << "." << endl;
				cout << "Su plan en vigencia es el numero: " << plan << endl;
				//cout << "ULTIMO PAGO HACE: " << diferencia << " DIAS." << endl;
				cout << "Dias abonados restantes: " << dias_rest << endl; cout << endl;
			}
		}while(doc != "-1");
	} 
	else{
		int cual; 						/// -MENU ADMINISTRACION- 
		cout << "Presione 1 para agregar un nuevo socio: " << endl;
		cout << "Presione 2 para eliminar un contacto: " << endl;
		cout << "Presione 3 para ver informacion de un socio: " << endl;
		cout << "Presione 4 para actualizar un socio." << endl;
		cout << "Presione 5 para buscar un socio por el nombre." << endl;
		cout << "Presione 6 para efectuar el pago de un socio." << endl;
		cout << "Presione 7 para consultar el ultimo pago de un socio. " << endl;
		cout << "Presione 8 para consultar el historial de pago de un socio. " << endl;
		cout << "Presione 9 para consultar los pagos hechos entre fechas." << endl;
		cout << "Presione 10 para ingresar como usuario." << endl;
		cout << "Presione 11 para consultar pagos segun un plan." << endl;
		cout << "Presione 12 para leer el registro de ingresos." << endl;
		cout << "Presione -1 para salir." << endl;
		
		do{
			cout<<"Ingrese una opcion: "<<endl;
			cin >> cual;
			switch(cual){
			case 1:{
				string name,last_n,docu,adress,email,number;
				int day,month,year;
				cout<<"Ingrese el nombre del cliente:"<<endl;
				cin.ignore();
				getline(cin,name);
				while(new_socio.ValidarNombreyApellido(name) == false){
					cout<<"El nombre ingresado no es valido. Intente nuevamente: "<<endl;
					cin.ignore();
					getline(cin,name);
				}
				new_socio.ModificarNombre(name);
				cout<<"Ingrese el apellido del cliente:"<<endl;
				getline(cin,last_n);
				while(new_socio.ValidarNombreyApellido(last_n) == false){
					cout<<"El apellido ingresado no es valido. Intente nuevamente: "<<endl;
					cin.ignore();
					getline(cin,last_n);
				}
				new_socio.ModificarApellido(last_n);
				cout<<"Ingrese la direccion del cliente:"<<endl;
				getline(cin,adress);
				while(new_socio.ValidarDireccion(adress) == false){
					cout<<"La direccion ingresada no es valida. Intente nuevamente: "<<endl;
					cin.ignore();
					getline(cin,adress);
				}
				new_socio.ModificarDireccion(adress);
				cout<<"Ingrese el documento del cliente:"<<endl;
				cin>>docu;
				while(new_socio.ValidarDni(docu) == false){
					cout<<"Dni ingresado no valido. Intente nuevamente: "<<endl;
					cin>>docu;
				}
				new_socio.ModificarDocumento(docu);
				cout<<"Ingrese el email del cliente:"<<endl;
				cin>>email;
				while(new_socio.ValidarEmail(email) == false){
					cout<<"El email ingresado no tiene @.Intente nuevamente: "<<endl;
					cin>>email;
				}
				new_socio.ModificarEmail(email);
				cout<<"Ingrese el telefono del cliente:"<<endl;
				cin>>number;
				while(new_socio.ValidarTelefono(number) == false){
					cout<<"Telefono ingresado no valido. Intente nuevamente: "<<endl;
					cin>>number;
				}
				new_socio.ModificarTelefono(number);
				cout<<"Ingrese dia, mes y anio de nacimiento del cliente:"<<endl;
				cin>>day;
				while(new_socio.ValidarFechaDia(day) == false){
					cout<<"Dia no valido.Intente nuevamente: "<<endl;
					cin>>day;
				}
				cin>>month;
				while(new_socio.ValidarFechaMes(month) == false){
					cout<<"Mes no valido.Intente nuevamente: "<<endl;
					cin>>month;
				}
				cin>>year;
				while(new_socio.ValidarFechaAnio(year) == false){
					cout<<"Año no valido.Intente nuevamente: "<<endl;
					cin>>year;
				}
				new_socio.ModificarFechaNac(day,month,year);
				Socios.AgregarSocio(new_socio);
				Socios.Guardar();
				break;}
			case 2:{	int socio_eliminar;
			cout<<"Ingrese el numero de socio que desea eliminar: ";
			cin>>socio_eliminar;
			socio_eliminar=socio_eliminar-1;
			Socios.EliminarSocio(socio_eliminar);
			Socios.Guardar();
			y.BorrarPagoSocio(socio_eliminar);
			for(int i=0;i<Socios.CantidadDatos();i++){
				Persona &p=Socios[i];
				cout<<"Socio " << i+1 << ": " << p.VerNombre()<< " " << p.VerApellido() << endl;
			}
			break;
				}
			case 3:{
				cout << "¿Que socio quiere ver? "; int opc_ingresada; cin >> opc_ingresada;
				opc_ingresada=opc_ingresada-1;
				if(opc_ingresada+1>Socios.CantidadDatos()){cout << "El socio indicado no existe." << endl; break;} else{
					Persona &p=Socios[opc_ingresada];
					cout<<"Nombres: "<<p.VerNombre()<<endl;
					cout<<"Apellido: "<<p.VerApellido()<<endl;
					cout<<"Documento: "<<p.VerDocumento()<<endl;
					cout<<"E-mail: "<<p.VerEmail()<<endl;
					cout<<"Telefono: "<<p.VerTelefono()<<endl;
					cout<<"Direccion: "<<p.VerDireccion()<<endl;
					cout<<"Fecha de Nacimiento: "
						<<setw(2)<<p.VerDiaNac()<<"/"
						<<setw(2)<<p.VerMesNac()<<"/"
						<<setw(4)<<p.VerAnioNac()<<endl; 
					break;
				}
			}
			case 4:{ int numero_socio_modificar;
			cout<<"Ingrese el numero de el socio que desea modificar: ";
			cin>>numero_socio_modificar;
			numero_socio_modificar=numero_socio_modificar-1;
			Persona &p=Socios[numero_socio_modificar];
			cout<<"Nombres: "<<p.VerNombre()<<endl;
			cout<<"Apellido: "<<p.VerApellido()<<endl;
			int num_modificar;
			cout<<"Ingrese 1 si desea modificar el nombre, 2 el apellido, 3 el documento, 4 el email,"<<endl<<" 5 el telefono, 6 la direccion y 7 la fecha de nacimiento: ";
			cin>>num_modificar; string nuevo;
			if(num_modificar==1){
				cout<<"Ingrese el nuevo nombre: ";
				cin.ignore();
				getline(cin,nuevo);
				while(p.ValidarNombreyApellido(nuevo) == false){
					cout<<"El apellido ingresado no es valido. Intente nuevamente: "<<endl;
					cin.ignore();
					getline(cin,nuevo);
				}
				p.ModificarNombre(nuevo);
				Socios.Guardar();
			}
			if(num_modificar==2){
				cout<<"Ingrese el nuevo apellido: ";
				getline(cin,nuevo);
				while(p.ValidarNombreyApellido(nuevo) == false){
					cout<<"El apellido ingresado no es valido. Intente nuevamente: "<<endl;
					cin.ignore();
					getline(cin,nuevo);
				}
				p.ModificarApellido(nuevo);
				Socios.Guardar();
			}
			if(num_modificar==3){
				cout<<"Ingrese el nuevo documento: ";
				cin>>nuevo;
				while(p.ValidarDni(nuevo) == false){
					cout<<"Dni no valido. Intente nuevamente: "<<endl;
					cin>>nuevo;
				}
				p.ModificarDocumento(nuevo);
				Socios.Guardar();
			}
			if(num_modificar==4){
				cout<<"Ingrese el nuevo email: ";
				cin>>nuevo;
				while(p.ValidarEmail(nuevo) == false){
					cout<<"El email ingresado no tiene @.Intente nuevamente: "<<endl;
					cin>>nuevo;
				}
				p.ModificarEmail(nuevo);
				Socios.Guardar();
			}
			if(num_modificar==5){
				cout<<"Ingrese el nuevo telefono: ";
				cin>>nuevo;
				while(p.ValidarTelefono(nuevo) == false){
					cout<<"Telefono ingresado no valido. Intente nuevamente: "<<endl;
					cin>>nuevo;
				}
				p.ModificarTelefono(nuevo);
				Socios.Guardar();
			} 
			if(num_modificar==6){
				cout<<"Ingrese la nueva direccion: ";
				getline(cin,nuevo);
				while(p.ValidarDireccion(nuevo) == false){
					cout<<"La direccion ingresada no es valida. Intente nuevamente: "<<endl;
					cin.ignore();
					getline(cin,nuevo);
				}
				p.ModificarDireccion(nuevo);
				Socios.Guardar();
			} 
			if(num_modificar==7){
				int d,m,a;
				cout<<"Ingrese la nueva fecha de nacimiento: "<<endl;
				cin>>d;
				while(p.ValidarFechaDia(d) == false){
					cout<<"El dia ingresado no es valido.Intente nuevamente: "<<endl;
					cin>>d;
				}
				cin>>m;
				while(p.ValidarFechaMes(m) == false){
					cout<<"Mes no valido.Intente nuevamente: "<<endl;
					cin>>m;
				}
				cin>>a;
				while(p.ValidarFechaAnio(a) == false){
					cout<<"Año no valido.Intente nuevamente: "<<endl;
					cin>>a;
				}
				p.ModificarFechaNac(d,m,a);
				Socios.Guardar();
			} 
			break;
				}
			
			case 5:{
				string n;
				cout << "Ingrese el nombre a buscar: "; 
				cin.ignore(); getline(cin,n); 
				int socio_buscado = Socios.BuscarApellidoYNombre(n,0);
				if(socio_buscado == -1){
					cout << "El socio indicado no existe." << endl;
					break;
				}
				Persona &p=Socios[socio_buscado];
				cout<<"Nombres: "<<p.VerNombre()<<endl;
				cout<<"Apellido: "<<p.VerApellido()<<endl;
				cout<<"Documento: "<<p.VerDocumento()<<endl;
				cout<<"E-mail: "<<p.VerEmail()<<endl;
				cout<<"Telefono: "<<p.VerTelefono()<<endl;
				cout<<"Direccion: "<<p.VerDireccion()<<endl;
				cout<<"Fecha de Nacimiento: "
					<<setw(2)<<p.VerDiaNac()<<"/"
					<<setw(2)<<p.VerMesNac()<<"/"
					<<setw(4)<<p.VerAnioNac()<<endl;
				break;
			}
				
			case 6:{ 
					int num_socio_pago, plan_pago;
					cout<<"Ingrese el numero de socio que efectua el pago: "<<endl;
					cin>>num_socio_pago; num_socio_pago=num_socio_pago-1;
					cout<<"Ingrese el numero de plan: "<<endl;
					cin>>plan_pago;
					y.Pagar(num_socio_pago, plan_pago);
					break;
				}
					
			case 7: {int s; cout << "Ingrese numero de socio a consultar: "; cin >> s; y.ConsultarUltimoPago(s-1); break;}
			//		cout << "Consola " << d <<"/"<< m <<"/"<< a << endl; break;}
			case 8: { 	
				int socio;  
				cout << "Ingrese el socio a consultar historial de pago: "; cin >> socio;
				y.ConsultarHistorialPago(socio-1);
				break;
			}
			
			case 9: {
				int dia_inicial,dia_final,mes_inicial,mes_final,anio_inicial,anio_final;
				cout<<"Ingrese desde que dia hasta que dia quiere ver los pagos: "<<endl;
				cin>>dia_inicial;
				cin>>dia_final;
				cout<<"Ingrese desde que mes hasta que mes quiere ver los pagos: "<<endl;
				cin>>mes_inicial;
				cin>>mes_final;
				cout<<"Ingrese desde que anio hasta que anio quiere ver los pagos: "<<endl;
				cin>>anio_inicial;
				cin>>anio_final;
				y.ConsultarPagosPorFecha(dia_inicial,mes_inicial,anio_inicial,dia_final,mes_final,anio_final);
				break;
			}
			case 10:{cout << "Ingrese documento: ";
			string doc;
			int diferencia,plan,dias_rest,socio;
			cin>>doc;
			tie(socio,plan,diferencia,dias_rest)=y.Ingresar(doc);						///SUMARLE UNO AL SOCIO QUE TE TIRA
			cout << "SOCIO: " << socio+1 << endl;
			cout << "PLAN: " << plan << endl;
			cout << "ULTIMO PAGO HACE: " << diferencia << " DIAS." << endl;
			cout << "DIAS RESTANTES: " << dias_rest << endl; break;}
			case 11:{cout << "Ingrese plan a consultar: "; int p; cin >> p;
			y.ConsultarPagosPorPlan(p); break;}
			case 12:{y.LeerRegistroIngreso();}
			}
			
		} while (cual != -1);}	
	
	return 0;
}


