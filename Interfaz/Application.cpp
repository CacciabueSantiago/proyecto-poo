#include <wx/image.h>
#include "Application.h"
#include "ListaSocios.h"
#include "HijaPrincipal.h"

IMPLEMENT_APP(Application)

bool Application::OnInit() {
	m_ListaSocios = new ListaSocios("ListaSocios.dat");
	HijaPrincipal *win = new HijaPrincipal(m_ListaSocios);
	win->Show();
	return true;
}

