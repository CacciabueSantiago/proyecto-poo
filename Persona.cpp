#include <string>
#include "Persona.h"
#include <cstring>
#include <fstream>
#include <cctype>
#include <iostream>
#include <ctype.h>
using namespace std;

Persona::Persona(string a_nombre, string a_apellido, string a_documento, string a_telefono,
				 string a_direccion, string a_email, 
				 int a_dia, int a_mes, int a_anio) {
	m_nombre = a_nombre;
	m_apellido = a_apellido;
	m_documento = a_documento;
	m_telefono = a_telefono;
	m_email = a_email;
	m_direccion = a_direccion;
	m_dianac = a_dia;
	m_mesnac = a_mes;
	m_anionac = a_anio;
}

string Persona::VerNombre() const {
	return m_nombre; 
}

string Persona::VerApellido() const {
	return m_apellido; 
}

string Persona::VerDocumento() const {
	return m_documento;
}

string Persona::VerDireccion() const { 
	return m_direccion; 
}

string Persona::VerTelefono() const { 
	return m_telefono; 
}

string Persona::VerEmail() const { 
	return m_email; 
}

int Persona::VerDiaNac() const { 
	return m_dianac; 
}

int Persona::VerMesNac() const { 
	return m_mesnac; 
}

int Persona::VerAnioNac() const { 
	return m_anionac; 
}

void Persona::ModificarNombre(string a_nombre) { 
	m_nombre=a_nombre; 
	
}

void Persona::ModificarApellido(string a_apellido) { 
	m_apellido=a_apellido; 
}

void Persona::ModificarDocumento(string a_documento) { 
	m_documento=a_documento; 
}

void Persona::ModificarDireccion(string a_direccion) { 
	m_direccion=a_direccion; 
}

void Persona::ModificarTelefono(string a_telefono) { 
	m_telefono=a_telefono; 
}

void Persona::ModificarEmail(string a_email) { 
	m_email=a_email; 
}

void Persona::ModificarFechaNac(int a_dia, int a_mes, int a_anio) { 
	m_anionac=a_anio; 
	m_mesnac=a_mes; 
	m_dianac=a_dia; 
}


void Persona::GuardarEnBinario(ofstream &archivo) {
	StructPersona reg;
	strcpy(reg.nombre,m_nombre.c_str());
	strcpy(reg.apellido,m_apellido.c_str());
	strcpy(reg.documento,m_documento.c_str());
	strcpy(reg.telefono,m_telefono.c_str());
	strcpy(reg.direccion,m_direccion.c_str());
	strcpy(reg.email,m_email.c_str());
	reg.anio_nac=m_anionac;
	reg.mes_nac=m_mesnac;
	reg.dia_nac=m_dianac;
	archivo.write((char*)&reg,sizeof(reg));
}

void Persona::LeerDesdeBinario(ifstream &archivo) {
	StructPersona reg;
	archivo.read((char*)&reg,sizeof(reg));
	m_nombre = reg.nombre;
	m_apellido = reg.apellido;     				
	m_documento = reg.documento;
	m_telefono = reg.telefono;
	m_direccion = reg.direccion;
	m_email = reg.email;
	m_anionac = reg.anio_nac;
	m_mesnac = reg.mes_nac;
	m_dianac = reg.dia_nac;
}

bool Persona::ValidarNombreyApellido(string nombre_apellido){
	if(nombre_apellido.empty()){
		return false;
	}
	int tam = nombre_apellido.length();
	char *a = new char[tam];
	for(int i=0;i<tam;i++) { 
		a[i] = nombre_apellido[i];
	}
	for(int i=0;i<tam;i++) { 
		if(isdigit(a[i])){
			return false;
		}
	}
	return true;
}

bool Persona::ValidarDni(string dni){
	if(dni.empty()){
		return false;
	}
	int tam = dni.size();
	for(int i=0;i<tam;i++) { 
		if(dni[i]<=48 || dni[i]>= 58){
			return false;
		}else{
			if(((dni.length()>8) or (dni.length()<8))){
				return false;
			}else{
				return true;
			}
		}
	}
	return true;
}

bool Persona::ValidarTelefono(string tel){
	if(tel.empty()){
		return false;
	}
	int tam = tel.size();
	for(int i=0;i<tam;i++) { 
		if(tel[i]<=48 || tel[i]>= 58){
			return false;
		}else{
			if(((tel.length()<7) or (tel.length()>12))){
				return false;
			}else{
				return true;
			}
		}
	}
	return true;
}

bool Persona::ValidarDireccion(string v_direccion){
	if(v_direccion.empty()){
		return false;
	}
	return true;
}


bool Persona::ValidarFechaDia(int v_dia){
	if(v_dia < 1 or v_dia > 31){
		return false;
	}
	return true;
}

bool Persona::ValidarFechaMes(int v_mes){
	if(v_mes < 1 or v_mes >12){
		return false;
	}
	return true;
}

bool Persona::ValidarFechaAnio(int v_anio){
	if(v_anio <1900){
		return false;
	}
	return true;
}

bool Persona::ValidarEmail(string v_email){
	if(v_email.empty()){
		return false;
	}
	size_t a = v_email.find("@");
	if(a==string::npos){
		return false;
	}
	return true;
}


//string Persona::ValidarDatos() {
//	string errores;
//	if (m_nombre.size()==0) errores+="El nombre no puede estar vacio\n";
//	if (m_nombre.size()>256) errores+="El nombre es demasiado largo\n";
//	if (m_apellido.size()==0) errores+="El apellido no puede estar vacio\n";
//	if (m_apellido.size()>256) errores+="El apellido es demasiado largo\n";
//	if (m_telefono.size()>256) errores+="El telefono es demasiado largo\n";
//	if (m_direccion.size()>256) errores+="La direccion es demasiado largo\n";
//	if (m_email.size()>256) errores+="El e-mail es demasiado largo\n";
//	if (m_dianac<0 || m_dianac>31) errores+="El dia de nacimiento debe estar entre 1 y 31, o vacio\n";
//	if (m_mesnac<0 || m_mesnac>12) errores+="El mes de nacimiento debe estar entre 1 y 12, o vacio\n";
//	if (m_anionac!=0 && m_anionac<=1900) errores+="El año de nacimiento no puede ser menor a 1900\n";
//	return errores;
//}
