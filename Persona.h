#ifndef PERSONA_H
#define PERSONA_H
#include <string>
using namespace std;

struct StructPersona {
	char nombre[256];
	char apellido[256];
	char documento[256];
	char telefono[256];
	char direccion[256];
	char email[256];
	int dia_nac, mes_nac, anio_nac;
};

class Persona {
private:
	string m_nombre;
	string m_apellido;
	string m_documento;
	string m_direccion;
	string m_email;
	string m_dni;
	string m_telefono;
	int m_dianac;
	int m_mesnac;
	int m_anionac;
public:
	Persona(string a_nombre="", string a_apellido="", string documento="", string a_telefono="",
		string a_direccion="", string a_email="", 
		int a_dia=0, int a_mes=0, int a_anio=0);

	string VerNombre() const; 
	string VerApellido() const;
	string VerDocumento() const;
	string VerDireccion() const; 
	string VerTelefono() const; 
	string VerEmail() const; 
	int VerDiaNac() const;
	int VerMesNac() const;
	int VerAnioNac() const;
	
	void ModificarNombre(string a_nombre); 
	void ModificarApellido(string a_apellido); 
	void ModificarDocumento(string a_documento);
	void ModificarDireccion(string a_direccion); 
	void ModificarTelefono(string a_telefono);
	void ModificarEmail(string a_email); 
	void ModificarFechaNac(int a_dia, int a_mes, int a_anio); 
	
	void GuardarEnBinario(ofstream &archivo);
	
	void LeerDesdeBinario(ifstream &archivo);
	
	bool ValidarNombreyApellido(string nombre_apellido);
	bool ValidarDni(string dni);
	bool ValidarTelefono(string tel);
	bool ValidarEmail(string v_email);
	bool ValidarDireccion(string v_direccion);
	
	/*bool ValidarFechaNac(int v_dia, int v_mes, int v_anio);*/
	
	bool ValidarFechaDia(int v_dia);
	bool ValidarFechaMes(int v_mes);
	bool ValidarFechaAnio(int v_anio);
	
//	string ValidarDatos();
};

#endif

