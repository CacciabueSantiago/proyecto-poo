#ifndef PAGO_H
#define PAGO_H
#include <tuple>
#include <vector>
#include "ListaSocios.h"

using namespace std;

struct s_pago{
	int s_socio,s_plan,dia_pago,mes_pago,anio_pago;
};

struct reg_ingreso{
	string s_doc;
	int dia_ing,mes_ing,anio_ing;
	int hora_ing,min_ing,seg_ing;
};
class Pago : public ListaSocios{
public:
	Pago(string nombre_archivo);
	void Pagar(int socio, int plan);
	tuple<int,int,int,int> Ingresar(string doc);
	
	void ConsultarUltimoPago(int socio);
	int ConsultarPlan(int socio);
	int ConsultarDiferencia(int socio);
	void ConsultarHistorialPago(int socio);
	void ConsultarPagosPorFecha(int c_diai,int c_mesi,int c_anioi,int c_diaf,int c_mesf,int c_aniof);
	void ConsultarPagosPorPlan(int plan);
	void LeerRegistroIngreso();
	
	void BorrarPagoSocio(int socio);
	int CalcularDiferencia(int socio);
	
	void ActualizarVector();
	
	
	tuple<int,int,int> Horario();
	
	bool EncontrarDni(string doc);
	
	void Prueba();
private:
	vector<s_pago> v;
	s_pago x; reg_ingreso z;
	vector<s_pago>temp;
	
};

#endif

