#include "ListaSocios.h"
#include <fstream>
#include "Minusculas.h"
#include <iostream>
using namespace std;

ListaSocios::ListaSocios(string m_nombre_archivo) {
	nombre_archivo = m_nombre_archivo;
	ifstream archivo(nombre_archivo.c_str(),ios::binary|ios::ate);
	if(archivo.is_open()){
		int tamanio_archivo = archivo.tellg();
		int cantidad_personas = tamanio_archivo/sizeof(StructPersona);
		ArregloPersonas.resize(cantidad_personas);
		archivo.seekg(0);
		for(int i=0;i<cantidad_personas;i++) 
			ArregloPersonas[i].LeerDesdeBinario(archivo);
		archivo.close();	
	}
}

bool ListaSocios::Guardar(){
	ofstream archivo(nombre_archivo.c_str(), ios::binary|ios::trunc);
	if(!archivo.is_open()) return false;
	int cantidad_personas = CantidadDatos();
	for(int i=0;i<cantidad_personas;i++) 
		ArregloPersonas[i].GuardarEnBinario(archivo);
	archivo.close();
	return true;
}

int ListaSocios::DevolverNumeroSocio(string dni){
	for(size_t i=0;i<ArregloPersonas.size();i++) { 
		if(ArregloPersonas[i].VerDocumento() == dni){
			return i;
		}
	}
	return -1;
}

string ListaSocios::DevolverNombreSocio(int soc){
	return ArregloPersonas[soc].VerNombre();
}

Persona &ListaSocios::operator[](int i){
	return ArregloPersonas[i];
}

int ListaSocios::CantidadDatos(){
	return ArregloPersonas.size();
}

void ListaSocios::AgregarSocio(const Persona &p){
	ArregloPersonas.push_back(p);
}

void ListaSocios::EliminarSocio(int i){
	ArregloPersonas.erase(ArregloPersonas.begin()+i);
}

int ListaSocios::BuscarApellidoYNombre(string parte, int pos_desde) {
	pasar_a_minusculas(parte);
	int cant = CantidadDatos();
	for (int i=pos_desde;i<cant;i++) {
		Persona &p = ArregloPersonas[i];
		string apenom = p.VerApellido()+", "+p.VerNombre();
		pasar_a_minusculas(apenom);
		if (apenom.find(parte,0)!= string::npos)
			return i;
	}
	//return NO_SE_ENCUENTRA;
		return -1;
}

